﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arakara.Battle
{
    public enum Animations
    {
        Idle,
        Attack1,
        Attack2,
        Hit,
        Walk,
        Run,
        Die,
        Event1,
        Event2,
        Support1,
        Support2
    }
}
